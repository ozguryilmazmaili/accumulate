// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package message

import (
	"context"

	"github.com/multiformats/go-multiaddr"
	"gitlab.com/accumulatenetwork/accumulate/pkg/api/v3"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/messaging"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
)

// Client is a binary message client for API v3.
type Client struct {
	Transport Transport
}

// A Router determines the address a message should be routed to.
type Router interface {
	Route(Message) (multiaddr.Multiaddr, error)
}

// Ensure Client satisfies the service definitions.
var _ api.NodeService = (*Client)(nil)
var _ api.ConsensusService = (*Client)(nil)
var _ api.NetworkService = (*Client)(nil)
var _ api.MetricsService = (*Client)(nil)
var _ api.Querier = (*Client)(nil)
var _ api.Submitter = (*Client)(nil)
var _ api.Validator = (*Client)(nil)
var _ api.Faucet = (*Client)(nil)

// NodeInfo implements [api.NodeService.NodeInfo].
func (c *Client) NodeInfo(ctx context.Context, opts NodeInfoOptions) (*api.NodeInfo, error) {
	// Wrap the request as a NodeStatusRequest and expect a NodeStatusResponse,
	// which is unpacked into a NodeInfo
	return typedRequest[*NodeInfoResponse, *api.NodeInfo](c, ctx, &NodeInfoRequest{NodeInfoOptions: opts})
}

// FindService implements [api.NodeService.FindService].
func (c *Client) FindService(ctx context.Context, opts FindServiceOptions) ([]*api.FindServiceResult, error) {
	// Wrap the request as a NodeStatusRequest and expect a NodeStatusResponse,
	// which is unpacked into a FindServiceResult
	return typedRequest[*FindServiceResponse, []*api.FindServiceResult](c, ctx, &FindServiceRequest{FindServiceOptions: opts})
}

// ConsensusStatus implements [api.NodeService.ConsensusStatus].
func (c *Client) ConsensusStatus(ctx context.Context, opts ConsensusStatusOptions) (*api.ConsensusStatus, error) {
	// Wrap the request as a NodeStatusRequest and expect a NodeStatusResponse,
	// which is unpacked into a ConsensusStatus
	return typedRequest[*ConsensusStatusResponse, *api.ConsensusStatus](c, ctx, &ConsensusStatusRequest{ConsensusStatusOptions: opts})
}

// NetworkStatus implements [api.NetworkService.NetworkStatus].
func (c *Client) NetworkStatus(ctx context.Context, opts NetworkStatusOptions) (*api.NetworkStatus, error) {
	// Wrap the request as a NetworkStatusRequest and expect a
	// NetworkStatusResponse, which is unpacked into a NetworkStatus
	return typedRequest[*NetworkStatusResponse, *api.NetworkStatus](c, ctx, &NetworkStatusRequest{NetworkStatusOptions: opts})
}

// Metrics implements [api.MetricsService.Metrics].
func (c *Client) Metrics(ctx context.Context, opts api.MetricsOptions) (*api.Metrics, error) {
	// Wrap the request as a MetricsRequest and expect a MetricsResponse, which
	// is unpacked into Metrics.
	req := &MetricsRequest{MetricsOptions: opts}
	return typedRequest[*MetricsResponse, *api.Metrics](c, ctx, req)
}

// Query implements [api.Querier.Query].
func (c *Client) Query(ctx context.Context, scope *url.URL, query api.Query) (api.Record, error) {
	// Wrap the request as a QueryRequest and expect a QueryResponse, which is
	// unpacked into a Record
	req := &QueryRequest{Scope: scope, Query: query}
	return typedRequest[*RecordResponse, api.Record](c, ctx, req)
}

// Submit implements [api.Submitter.Submit].
func (c *Client) Submit(ctx context.Context, envelope *messaging.Envelope, opts api.SubmitOptions) ([]*api.Submission, error) {
	// Wrap the request as a SubmitRequest and expect a SubmitResponse, which is
	// unpacked into Submissions
	req := &SubmitRequest{Envelope: envelope, SubmitOptions: opts}
	return typedRequest[*SubmitResponse, []*api.Submission](c, ctx, req)
}

// Validate implements [api.Validator.Validate].
func (c *Client) Validate(ctx context.Context, envelope *messaging.Envelope, opts api.ValidateOptions) ([]*api.Submission, error) {
	// Wrap the request as a ValidateRequest and expect a ValidateResponse,
	// which is unpacked into Submissions
	req := &ValidateRequest{Envelope: envelope, ValidateOptions: opts}
	return typedRequest[*ValidateResponse, []*api.Submission](c, ctx, req)
}

// Faucet implements [api.Faucet.Faucet].
func (c *Client) Faucet(ctx context.Context, account *url.URL, opts api.FaucetOptions) (*api.Submission, error) {
	// Wrap the request as a FaucetRequest and expect a FaucetResponse,
	// which is unpacked into Submissions
	req := &FaucetRequest{Account: account, FaucetOptions: opts}
	return typedRequest[*FaucetResponse, *api.Submission](c, ctx, req)
}

// typedRequest executes a round-trip call, sending the request and expecting a
// response of the given type.
func typedRequest[M response[T], T any](c *Client, ctx context.Context, req Message) (T, error) {
	var typRes M
	var errRes *ErrorResponse
	err := c.Transport.RoundTrip(ctx, []Message{req}, func(res, _ Message) error {
		switch res := res.(type) {
		case *ErrorResponse:
			errRes = res
			return nil
		case M:
			typRes = res
			return nil
		default:
			return errors.Conflict.WithFormat("invalid response type %T", res)
		}
	})
	var z T
	if err != nil {
		return z, err
	}
	if errRes != nil {
		return z, errRes.Error
	}
	return typRes.rval(), nil
}

// response exists for typedRequest.
type response[T any] interface {
	Message
	rval() T
}

func (r *NodeInfoResponse) rval() *api.NodeInfo               { return r.Value } //nolint:unused
func (r *FindServiceResponse) rval() []*api.FindServiceResult { return r.Value } //nolint:unused
func (r *ConsensusStatusResponse) rval() *api.ConsensusStatus { return r.Value } //nolint:unused
func (r *NetworkStatusResponse) rval() *api.NetworkStatus     { return r.Value } //nolint:unused
func (r *MetricsResponse) rval() *api.Metrics                 { return r.Value } //nolint:unused
func (r *RecordResponse) rval() api.Record                    { return r.Value } //nolint:unused
func (r *SubmitResponse) rval() []*api.Submission             { return r.Value } //nolint:unused
func (r *ValidateResponse) rval() []*api.Submission           { return r.Value } //nolint:unused
func (r *FaucetResponse) rval() *api.Submission               { return r.Value } //nolint:unused
func (r *EventMessage) rval() []api.Event                     { return r.Value } //nolint:unused

func (r *PrivateSequenceResponse) rval() *api.MessageRecord[messaging.Message] { //nolint:unused
	return r.Value
}

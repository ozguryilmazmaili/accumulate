- name: Batch
  type: entity
  root: true
  customCommit: true
  fields:
  - name: done
    type: bool
  - name: writable
    type: bool
  - name: id
    type: string
  - name: nextChildId
    type: int
  - name: parent
    type: Batch
    pointer: true
  - name: observer
    type: Observer
  - name: kvstore
    type: storage.KeyValueTxn
  attributes:
  - name: BPT
    type: other
    dataType: bpt.BPT
    constructor: newBPT

  - name: Account
    type: entity
    customCommit: true
    private: true
    parameters:
    - name: Url
      type: url
      pointer: true
    attributes:
    - name: Url
      type: state
      dataType: url
      pointer: true
      private: true
    - name: Main
      type: state
      dataType: protocol.Account
      union: true
    - name: Pending
      type: state
      dataType: txid
      emptyIfMissing: true
      pointer: true
      collection: set
    - name: SyntheticForAnchor
      type: state
      parameters:
      - name: Anchor
        type: hash
      dataType: txid
      emptyIfMissing: true
      pointer: true
      collection: set
    - name: Directory
      type: state
      collection: set
      dataType: url
      pointer: true
      emptyIfMissing: true

    - name: Transaction
      type: entity
      parameters:
      - name: Hash
        type: hash
      attributes:
      - name: Payments
        description: records the hashes of credit payment messages
        type: state
        dataType: hash
        collection: set
      - name: Votes
        description: lists authorities that have voted on this transaction
        type: state
        dataType: VoteEntry
        collection: set
        pointer: true
        comparator: compareVoteEntries
      - name: Signatures
        description: records the active set of signatures
        type: state
        dataType: SignatureSetEntry
        collection: set
        pointer: true
        comparator: compareSignatureSetEntries
      - name: AnchorSignatures
        description: records validators' anchor transaction signatures
        type: state
        dataType: protocol.KeySignature
        union: true
        collection: set
        comparator: compareAnchorSignatures
      - name: History
        type: index
        description: records the signature chain index of entries for the transaction
        dataType: uint
        collection: set

    # Chains
    - name: MainChain
      type: other
      dataType: Chain2
      hasChains: true
      pointer: true
    - name: ScratchChain
      type: other
      dataType: Chain2
      hasChains: true
      pointer: true
    - name: SignatureChain
      type: other
      dataType: Chain2
      hasChains: true
      pointer: true
    - name: RootChain
      type: other
      dataType: Chain2
      hasChains: true
      pointer: true
    - name: AnchorSequenceChain
      type: other
      dataType: Chain2
      hasChains: true
      pointer: true
    - name: MajorBlockChain
      type: other
      dataType: Chain2
      hasChains: true
      pointer: true
    - name: SyntheticSequenceChain
      type: other
      dataType: Chain2
      hasChains: true
      pointer: true
      private: true
      parameters:
      - name: Partition
        type: string
    - name: AnchorChain
      type: entity
      private: true
      parameters:
      - name: Partition
        type: string
      attributes:
      - name: Root
        type: other
        dataType: Chain2
        hasChains: true
        pointer: true
      - name: BPT
        type: other
        dataType: Chain2
        hasChains: true
        pointer: true

    # Indices
    - name: Chains
      type: index
      dataType: protocol.ChainMetadata
      pointer: true
      collection: set
    - name: SyntheticAnchors
      type: index
      dataType: hash
      collection: set
    - name: Data
      type: entity
      attributes:
      - name: Entry
        type: index
        collection: counted
        dataType: hash
      - name: Transaction
        type: index
        parameters:
        - name: EntryHash
          type: hash
        dataType: hash

  - name: Message
    type: entity
    parameters:
    - name: Hash
      type: hash
    attributes:
    - name: Main
      type: state
      dataType: messaging.Message
      union: true
      private: true
    - name: Cause
      type: index
      dataType: txid
      pointer: true
      collection: set
    - name: Produced
      type: index
      dataType: txid
      pointer: true
      collection: set
    - name: Signers
      type: index
      dataType: url
      pointer: true
      collection: set

  - name: Transaction
    type: entity
    private: true
    parameters:
    - name: Hash
      type: hash
    attributes:
    - name: Main
      type: state
      dataType: SigOrTxn
      pointer: true
    - name: Status
      type: state
      dataType: protocol.TransactionStatus
      emptyIfMissing: true
      pointer: true
    - name: Produced
      type: state
      dataType: txid
      pointer: true
      collection: set
    - name: Signatures
      type: state
      dataType: sigSetData
      pointer: true
      private: true
      emptyIfMissing: true
      parameters:
      - name: Signer
        type: url
        pointer: true

    # Indices
    - name: Chains
      type: index
      dataType: TransactionChainEntry
      pointer: true
      collection: set
      emptyIfMissing: true

  # System Indices
  - name: SystemData
    type: entity
    parameters:
    - name: Partition
      type: string
    attributes:
    - name: SyntheticIndexIndex
      # Indexes from a block index to a synthetic index chain index
      type: index
      dataType: uint
      parameters:
      - name: Block
        type: uint

- name: MerkleManager
  type: entity
  fields:
  - name: typ
    type: merkle.ChainType
  - name: name
    type: string
  - name: markPower
    type: int
  - name: markFreq
    type: int
  - name: markMask
    type: int
  attributes:
  - name: Head
    type: state
    emptyIfMissing: true
    dataType: MerkleState
    pointer: true
  - name: States
    type: state
    dataType: MerkleState
    pointer: true
    parameters:
    - name: Index
      type: uint
  - name: ElementIndex
    type: index
    dataType: uint
    parameters:
    - name: Hash
      type: bytes
  - name: Element
    type: index
    dataType: bytes
    parameters:
    - name: Index
      type: uint

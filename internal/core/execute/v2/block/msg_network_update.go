// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package block

import (
	"gitlab.com/accumulatenetwork/accumulate/internal/core/execute/internal"
	"gitlab.com/accumulatenetwork/accumulate/internal/database"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/messaging"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
)

func init() {
	registerSimpleExec[NetworkUpdate](&messageExecutors, internal.MessageTypeNetworkUpdate)
}

// NetworkUpdate constructs a transaction for the network update and queues it
// for processing.
type NetworkUpdate struct{}

func (NetworkUpdate) Validate(batch *database.Batch, ctx *MessageContext) (*protocol.TransactionStatus, error) {
	return nil, errors.InternalError.With("invalid attempt to validate an internal message")
}

func (NetworkUpdate) Process(batch *database.Batch, ctx *MessageContext) (*protocol.TransactionStatus, error) {
	msg, ok := ctx.message.(*internal.NetworkUpdate)
	if !ok {
		return nil, errors.InternalError.WithFormat("invalid message type: expected %v, got %v", internal.MessageTypeNetworkUpdate, ctx.message.Type())
	}

	txn := new(protocol.Transaction)
	txn.Header.Principal = msg.Account
	txn.Header.Initiator = msg.Cause
	txn.Body = msg.Body

	batch = batch.Begin(true)
	defer batch.Discard()

	// Record that the cause produced this update
	err := batch.Transaction(msg.Cause[:]).Produced().Add(txn.ID())
	if err != nil {
		return nil, errors.UnknownError.WithFormat("update cause: %w", err)
	}

	// Store the transaction
	err = batch.Message(txn.ID().Hash()).Main().Put(&messaging.TransactionMessage{Transaction: txn})
	if err != nil {
		return nil, errors.UnknownError.WithFormat("store transaction: %w", err)
	}

	// Store a fake payment
	pay := new(messaging.CreditPayment)
	pay.Payer = protocol.DnUrl().JoinPath(protocol.Network)
	pay.Cause = pay.Payer.WithTxID(msg.Cause)
	pay.Initiator = true
	pay.TxID = txn.ID()
	err = batch.Message(pay.Hash()).Main().Put(pay)
	if err != nil {
		return nil, errors.UnknownError.WithFormat("store payment: %w", err)
	}
	err = batch.Message(pay.Hash()).Cause().Add(pay.Cause)
	if err != nil {
		return nil, errors.UnknownError.WithFormat("store payment cause: %w", err)
	}

	err = batch.Account(msg.Account).
		Transaction(txn.ID().Hash()).
		Payments().
		Add(pay.Hash())
	if err != nil {
		return nil, errors.UnknownError.WithFormat("store payment hash: %w", err)
	}

	// Execute the transaction
	st, err := ctx.callMessageExecutor(batch, &messaging.TransactionMessage{Transaction: txn})
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}

	err = batch.Commit()
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}

	return st, nil
}

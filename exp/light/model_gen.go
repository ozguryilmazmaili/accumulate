// Copyright 2022 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package light

// GENERATED BY go run ./tools/cmd/gen-model. DO NOT EDIT.

//lint:file-ignore S1008,U1000 generated code

import (
	"encoding/hex"

	"gitlab.com/accumulatenetwork/accumulate/internal/database/record"
	"gitlab.com/accumulatenetwork/accumulate/internal/logging"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
)

type IndexDB interface {
	record.Record
	Account(url *url.URL) IndexDBAccount
	Partition(url *url.URL) IndexDBPartition
	Transaction(hash [32]byte) IndexDBTransaction
}

type indexDB struct {
	logger logging.OptionalLogger
	store  record.Store
	key    record.Key
	label  string

	account     map[indexDBAccountKey]*indexDBAccount
	partition   map[indexDBPartitionKey]*indexDBPartition
	transaction map[indexDBTransactionKey]*indexDBTransaction
}

type indexDBAccountKey struct {
	Url [32]byte
}

func keyForIndexDBAccount(url *url.URL) indexDBAccountKey {
	return indexDBAccountKey{record.MapKeyUrl(url)}
}

type indexDBPartitionKey struct {
	Url [32]byte
}

func keyForIndexDBPartition(url *url.URL) indexDBPartitionKey {
	return indexDBPartitionKey{record.MapKeyUrl(url)}
}

type indexDBTransactionKey struct {
	Hash [32]byte
}

func keyForIndexDBTransaction(hash [32]byte) indexDBTransactionKey {
	return indexDBTransactionKey{hash}
}

func (c *indexDB) Account(url *url.URL) IndexDBAccount {
	return record.FieldGetOrCreateMap(&c.account, keyForIndexDBAccount(url), func() *indexDBAccount {
		v := new(indexDBAccount)
		v.logger = c.logger
		v.store = c.store
		v.key = c.key.Append("Account", url)
		v.parent = c
		v.label = c.label + " " + "account" + " " + url.RawString()
		return v
	})
}

func (c *indexDB) Partition(url *url.URL) IndexDBPartition {
	return record.FieldGetOrCreateMap(&c.partition, keyForIndexDBPartition(url), func() *indexDBPartition {
		v := new(indexDBPartition)
		v.logger = c.logger
		v.store = c.store
		v.key = c.key.Append("Partition", url)
		v.parent = c
		v.label = c.label + " " + "partition" + " " + url.RawString()
		return v
	})
}

func (c *indexDB) Transaction(hash [32]byte) IndexDBTransaction {
	return record.FieldGetOrCreateMap(&c.transaction, keyForIndexDBTransaction(hash), func() *indexDBTransaction {
		v := new(indexDBTransaction)
		v.logger = c.logger
		v.store = c.store
		v.key = c.key.Append("Transaction", hash)
		v.parent = c
		v.label = c.label + " " + "transaction" + " " + hex.EncodeToString(hash[:])
		return v
	})
}

func (c *indexDB) Resolve(key record.Key) (record.Record, record.Key, error) {
	if len(key) == 0 {
		return nil, nil, errors.InternalError.With("bad key for index db")
	}

	switch key[0] {
	case "Account":
		if len(key) < 2 {
			return nil, nil, errors.InternalError.With("bad key for index db")
		}
		url, okUrl := key[1].(*url.URL)
		if !okUrl {
			return nil, nil, errors.InternalError.With("bad key for index db")
		}
		v := c.Account(url)
		return v, key[2:], nil
	case "Partition":
		if len(key) < 2 {
			return nil, nil, errors.InternalError.With("bad key for index db")
		}
		url, okUrl := key[1].(*url.URL)
		if !okUrl {
			return nil, nil, errors.InternalError.With("bad key for index db")
		}
		v := c.Partition(url)
		return v, key[2:], nil
	case "Transaction":
		if len(key) < 2 {
			return nil, nil, errors.InternalError.With("bad key for index db")
		}
		hash, okHash := key[1].([32]byte)
		if !okHash {
			return nil, nil, errors.InternalError.With("bad key for index db")
		}
		v := c.Transaction(hash)
		return v, key[2:], nil
	default:
		return nil, nil, errors.InternalError.With("bad key for index db")
	}
}

func (c *indexDB) IsDirty() bool {
	if c == nil {
		return false
	}

	for _, v := range c.account {
		if v.IsDirty() {
			return true
		}
	}
	for _, v := range c.partition {
		if v.IsDirty() {
			return true
		}
	}
	for _, v := range c.transaction {
		if v.IsDirty() {
			return true
		}
	}

	return false
}

func (c *indexDB) WalkChanges(fn record.WalkFunc) error {
	if c == nil {
		return nil
	}

	var err error
	for _, v := range c.account {
		record.FieldWalkChanges(&err, v, fn)
	}
	for _, v := range c.partition {
		record.FieldWalkChanges(&err, v, fn)
	}
	for _, v := range c.transaction {
		record.FieldWalkChanges(&err, v, fn)
	}
	return err
}

func (c *indexDB) Commit() error {
	if c == nil {
		return nil
	}

	var err error
	for _, v := range c.account {
		record.FieldCommit(&err, v)
	}
	for _, v := range c.partition {
		record.FieldCommit(&err, v)
	}
	for _, v := range c.transaction {
		record.FieldCommit(&err, v)
	}

	return err
}

type IndexDBAccount interface {
	record.Record
	DidIndexTransactionExecution() record.Set[[32]byte]
	DidLoadTransaction() record.Set[[32]byte]
	Chain(name string) IndexDBAccountChain
}

type indexDBAccount struct {
	logger logging.OptionalLogger
	store  record.Store
	key    record.Key
	label  string
	parent *indexDB

	didIndexTransactionExecution record.Set[[32]byte]
	didLoadTransaction           record.Set[[32]byte]
	chain                        map[indexDBAccountChainKey]*indexDBAccountChain
}

type indexDBAccountChainKey struct {
	Name string
}

func keyForIndexDBAccountChain(name string) indexDBAccountChainKey {
	return indexDBAccountChainKey{name}
}

func (c *indexDBAccount) DidIndexTransactionExecution() record.Set[[32]byte] {
	return record.FieldGetOrCreate(&c.didIndexTransactionExecution, func() record.Set[[32]byte] {
		return record.NewSet(c.logger.L, c.store, c.key.Append("DidIndexTransactionExecution"), c.label+" "+"did index transaction execution", record.Wrapped(record.HashWrapper), record.CompareHash)
	})
}

func (c *indexDBAccount) DidLoadTransaction() record.Set[[32]byte] {
	return record.FieldGetOrCreate(&c.didLoadTransaction, func() record.Set[[32]byte] {
		return record.NewSet(c.logger.L, c.store, c.key.Append("DidLoadTransaction"), c.label+" "+"did load transaction", record.Wrapped(record.HashWrapper), record.CompareHash)
	})
}

func (c *indexDBAccount) Chain(name string) IndexDBAccountChain {
	return record.FieldGetOrCreateMap(&c.chain, keyForIndexDBAccountChain(name), func() *indexDBAccountChain {
		v := new(indexDBAccountChain)
		v.logger = c.logger
		v.store = c.store
		v.key = c.key.Append("Chain", name)
		v.parent = c
		v.label = c.label + " " + "chain" + " " + name
		return v
	})
}

func (c *indexDBAccount) Resolve(key record.Key) (record.Record, record.Key, error) {
	if len(key) == 0 {
		return nil, nil, errors.InternalError.With("bad key for account")
	}

	switch key[0] {
	case "DidIndexTransactionExecution":
		return c.DidIndexTransactionExecution(), key[1:], nil
	case "DidLoadTransaction":
		return c.DidLoadTransaction(), key[1:], nil
	case "Chain":
		if len(key) < 2 {
			return nil, nil, errors.InternalError.With("bad key for account")
		}
		name, okName := key[1].(string)
		if !okName {
			return nil, nil, errors.InternalError.With("bad key for account")
		}
		v := c.Chain(name)
		return v, key[2:], nil
	default:
		return nil, nil, errors.InternalError.With("bad key for account")
	}
}

func (c *indexDBAccount) IsDirty() bool {
	if c == nil {
		return false
	}

	if record.FieldIsDirty(c.didIndexTransactionExecution) {
		return true
	}
	if record.FieldIsDirty(c.didLoadTransaction) {
		return true
	}
	for _, v := range c.chain {
		if v.IsDirty() {
			return true
		}
	}

	return false
}

func (c *indexDBAccount) WalkChanges(fn record.WalkFunc) error {
	if c == nil {
		return nil
	}

	var err error
	record.FieldWalkChanges(&err, c.didIndexTransactionExecution, fn)
	record.FieldWalkChanges(&err, c.didLoadTransaction, fn)
	for _, v := range c.chain {
		record.FieldWalkChanges(&err, v, fn)
	}
	return err
}

func (c *indexDBAccount) Commit() error {
	if c == nil {
		return nil
	}

	var err error
	record.FieldCommit(&err, c.didIndexTransactionExecution)
	record.FieldCommit(&err, c.didLoadTransaction)
	for _, v := range c.chain {
		record.FieldCommit(&err, v)
	}

	return err
}

type IndexDBAccountChain interface {
	record.Record
	Index() record.List[*protocol.IndexEntry]
}

type indexDBAccountChain struct {
	logger logging.OptionalLogger
	store  record.Store
	key    record.Key
	label  string
	parent *indexDBAccount

	index record.List[*protocol.IndexEntry]
}

func (c *indexDBAccountChain) Index() record.List[*protocol.IndexEntry] {
	return record.FieldGetOrCreate(&c.index, func() record.List[*protocol.IndexEntry] {
		return record.NewList(c.logger.L, c.store, c.key.Append("Index"), c.label+" "+"index", record.Struct[protocol.IndexEntry]())
	})
}

func (c *indexDBAccountChain) Resolve(key record.Key) (record.Record, record.Key, error) {
	if len(key) == 0 {
		return nil, nil, errors.InternalError.With("bad key for chain")
	}

	switch key[0] {
	case "Index":
		return c.Index(), key[1:], nil
	default:
		return nil, nil, errors.InternalError.With("bad key for chain")
	}
}

func (c *indexDBAccountChain) IsDirty() bool {
	if c == nil {
		return false
	}

	if record.FieldIsDirty(c.index) {
		return true
	}

	return false
}

func (c *indexDBAccountChain) WalkChanges(fn record.WalkFunc) error {
	if c == nil {
		return nil
	}

	var err error
	return err
}

func (c *indexDBAccountChain) Commit() error {
	if c == nil {
		return nil
	}

	var err error
	record.FieldCommit(&err, c.index)

	return err
}

type IndexDBPartition interface {
	record.Record
	Anchors() record.List[*AnchorMetadata]
}

type indexDBPartition struct {
	logger logging.OptionalLogger
	store  record.Store
	key    record.Key
	label  string
	parent *indexDB

	anchors record.List[*AnchorMetadata]
}

func (c *indexDBPartition) Anchors() record.List[*AnchorMetadata] {
	return record.FieldGetOrCreate(&c.anchors, func() record.List[*AnchorMetadata] {
		return record.NewList(c.logger.L, c.store, c.key.Append("Anchors"), c.label+" "+"anchors", record.Struct[AnchorMetadata]())
	})
}

func (c *indexDBPartition) Resolve(key record.Key) (record.Record, record.Key, error) {
	if len(key) == 0 {
		return nil, nil, errors.InternalError.With("bad key for partition")
	}

	switch key[0] {
	case "Anchors":
		return c.Anchors(), key[1:], nil
	default:
		return nil, nil, errors.InternalError.With("bad key for partition")
	}
}

func (c *indexDBPartition) IsDirty() bool {
	if c == nil {
		return false
	}

	if record.FieldIsDirty(c.anchors) {
		return true
	}

	return false
}

func (c *indexDBPartition) WalkChanges(fn record.WalkFunc) error {
	if c == nil {
		return nil
	}

	var err error
	record.FieldWalkChanges(&err, c.anchors, fn)
	return err
}

func (c *indexDBPartition) Commit() error {
	if c == nil {
		return nil
	}

	var err error
	record.FieldCommit(&err, c.anchors)

	return err
}

type IndexDBTransaction interface {
	record.Record
	Executed() record.Value[*EventMetadata]
}

type indexDBTransaction struct {
	logger logging.OptionalLogger
	store  record.Store
	key    record.Key
	label  string
	parent *indexDB

	executed record.Value[*EventMetadata]
}

func (c *indexDBTransaction) Executed() record.Value[*EventMetadata] {
	return record.FieldGetOrCreate(&c.executed, func() record.Value[*EventMetadata] {
		return record.NewValue(c.logger.L, c.store, c.key.Append("Executed"), c.label+" "+"executed", false, record.Struct[EventMetadata]())
	})
}

func (c *indexDBTransaction) Resolve(key record.Key) (record.Record, record.Key, error) {
	if len(key) == 0 {
		return nil, nil, errors.InternalError.With("bad key for transaction")
	}

	switch key[0] {
	case "Executed":
		return c.Executed(), key[1:], nil
	default:
		return nil, nil, errors.InternalError.With("bad key for transaction")
	}
}

func (c *indexDBTransaction) IsDirty() bool {
	if c == nil {
		return false
	}

	if record.FieldIsDirty(c.executed) {
		return true
	}

	return false
}

func (c *indexDBTransaction) WalkChanges(fn record.WalkFunc) error {
	if c == nil {
		return nil
	}

	var err error
	return err
}

func (c *indexDBTransaction) Commit() error {
	if c == nil {
		return nil
	}

	var err error
	record.FieldCommit(&err, c.executed)

	return err
}
